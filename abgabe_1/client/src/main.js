document.addEventListener('DOMContentLoaded', () => {
  
  document.getElementById('changeViewBtn').onclick = onChangeViewBtnClick
  document.getElementById('refreshBtn').onclick = onRefreshBtnClick
  document.getElementById('addBtn').onclick = onAddBtnClick

  getListItems(() => {
    addOnClickToListItems()
  })
})
