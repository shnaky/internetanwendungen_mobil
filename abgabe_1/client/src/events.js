const onRefreshBtnClick = () => {
  getListItems(() => {
    addOnClickToListItems()
  })
}

const onAddBtnClick = () => {
  const date = new Date()
  const day = date.getDay() < 10 ? `0${date.getDay()}` : `${date.getDay()}`

  const item = {
    name: 'test',
    owner: 'testowner.com',
    added: `${day}.${date.getMonth()}.${date.getFullYear()}`,
    numOfTags: 4,
    src: 'https://placeimg.com/100/100/nature'
  }

  addListItem(item)
  addOnClickToListItems()
}

const onChangeViewBtnClick = () => {
  const content = document.getElementById('content')
  const changeViewBtn = document.getElementById('changeViewBtn')
  const icons = changeViewBtn.getElementsByTagName('i')
  
  if (icons.length == 0) { return }

  let currentView = ''
  let iconClass = icons[0].className

  if (iconClass.includes('fa-th')) {
    icons[0].className = 'fas fa-list-ul fa-lg'
    currentView = 'track-list'
  } else if (iconClass.includes('fa-list-ul')) {
    icons[0].className = 'fas fa-th fa-lg'
    currentView = 'tile-list'
  }

  fadeOut(content, () => {
    if (currentView === 'track-list') {
      let tracks = content.getElementsByTagName('li')
      for (let track of tracks) {
        track.className = 'tile-li'
      }
    } else if (currentView === 'tile-list') {
      let tiles = content.getElementsByTagName('li')
      for (let tile of tiles) {
        tile.className = 'track-li'
      }
    }

    fadeIn(content, () => {
      'done'
    })
  })
}